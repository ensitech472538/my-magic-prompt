#!/bin/bash
source quit.sh
source ls.sh
source help.sh
source rm.sh
source rmdir.sh
source about.sh
source version.sh
source age.sh
source profil.sh
source pwd.sh
source cd.sh
source hour.sh
source open.sh
source httpget.sh
source auth.sh
source passw.sh
source smtp.sh
source rps.sh

cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    ls ) ls $2 $3;;
    help ) help;;
    rm ) rm $2 $3;;
    rmdir | rmd ) rmdir $2 $3;;
    about ) about;;
    version | --v | vers ) version;;
    age ) age;;
    profil ) profil;;
    pwd ) pwd;;
    cd ) cd $2;;
    hour ) hour;;
    open ) open $2;;
    httpget ) httpget;;
    passw ) passw;;
    smtp ) smtp;;
    rps ) rps;;
    * ) echo "Commande inexistante";;
  esac
}

main() {
  auth
  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    pwd=$(pwd)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mLoïc\033[m ~ ☠️ ~ ${pwd} ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
