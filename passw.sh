#!/bin/sh
passw(){
    echo "Veuillez saisir l'ancien mot de passe : "
    read -s old_pwd

    found=false
    index=-1
    for i in "${!account[@]}"; do
        fields=(${account[i]})
        if [ "$login" = "${fields[0]}" ]; then
            found=true
            index=$i
            break
        fi
    done

    if [ "$found" = true ]; then
        while [ "$old_pwd" != "${fields[1]}" ]; do
            echo "Mauvais mot de passe. Veuillez saisir l'ancien mot de passe : "
            read -s old_pwd
        done

        echo "Veuillez saisir le nouveau mot de passe : "
        read -s new_pwd

        while [ "$old_pwd" = "$new_pwd" ]; do
            echo "Veuillez saisir un mot de passe différent de l'ancien"
            read -s new_pwd
        done

        echo "Voulez vous vraiment changer votre mot de passe ? (O/n)"
        read response

        if [ "$response" = "O" ]; then
            # Mettez à jour le mot de passe dans le tableau account
            account[$index]="${fields[0]} $new_pwd ${fields[2]} ${fields[3]} ${fields[4]} ${fields[5]}"
            echo "Mot de passe changé avec succès. Veuillez vous reconnecter."
            auth
        else
            echo "Opération annulée."
        fi
    else
        echo "Compte introuvable."
    fi
}