#!/bin/sh
auth(){

    if [ "$found" = false ] || [ "$found" = "" ]; then
        account=(
            "llepie 1234 Loïc Lepie lepieloic94@gmail.com 21"
            "ddemoineret 1234 Denis Demoin denisdemoin@gmail.com 20"
            "tchar 1234 Tony Charbon tonycharbon@gmail.com 22"
        )
    fi

    echo "Possédez vous un compte ? (O/n)"
    read response

    if [ "$response" = "O" ]; then
        echo "Veuillez saisir votre login : "
        read login

        found=false
        index=-1
        for i in "${!account[@]}"; do
            fields=(${account[i]})
            if [ "$login" = "${fields[0]}" ]; then
                found=true
                index=$i
                break
            fi
        done

        if [ "$found" = true ]; then
            echo "Veuillez saisir votre mot de passe : "
            read -s mdp

            if [ "$mdp" = "${fields[1]}" ]; then
                echo "Vous êtes bien connecté !"
                profil=("Prénom : ${fields[2]} 🤖" "Nom de famille : ${fields[3]} 🤖" "Âge : ${fields[5]} 🤖" "Adresse mail : ${fields[4]} 🤖")
            else
                echo "Mot de passe incorrect."
                exit
            fi
        else
            echo "Compte introuvable."
            exit
        fi
    else
        echo "Créer un compte."
        echo "---------------"
        echo "Identifiant : "
        read login
        echo "Mot de passe : "
        read -s mdp
        echo "Prénom : "
        read firstname
        echo "Nom : "
        read lastname
        echo "Âge : "
        read age
        echo "Adresse mail : "
        read mail
        echo "---------------"
        echo "Votre compte a bien été créé, vous êtes connecté"
        account=("$login $mdp $firstname $lastname $mail $age")
        profil=("Prénom : $firstname 🤖" "Nom de famille : $lastname 🤖" "Âge : $age 🤖" "Adresse mail : $mail 🤖")
    fi
}
