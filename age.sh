#!/bin/sh
age(){
    echo "Veuillez saisir votre âge : "
    read age

    if [ "$age" -ge 18 ]; then
        echo "Vous êtes majeur : $age ans 👨"
    else
        echo "Vous êtes mineur : $age ans 👶"
    fi
}