#!/bin/bash
smtp(){
    while [ "$subject" = "" ]; do
        echo "Sujet du mail : "
        read subject
    done

    while [ "$to" = "" ]; do
        echo "À qui ? : "
        read to
    done

    while [ "$body" = "" ]; do
        echo "Corps du mail : "
        read body
    done

    password=$(cat config.txt)

    echo -e "Subject:$subject\n$body" | curl -sS --url "smtps://smtp.gmail.com:465" --ssl-reqd \
    --mail-from "lepieloic94@gmail.com" --mail-rcpt "$to" \
    --user "lepieloic94@gmail.com:$password" -T -
    
    if [ $? -eq 0 ]; then
        echo "L'e-mail a été envoyé avec succès !"
        subject=""
        to=""
        body=""
    else
        echo "Erreur lors de l'envoi de l'e-mail."
    fi
}
