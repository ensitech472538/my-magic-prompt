#!/bin/sh
who_begin(){
    arg1=$1
    arg2=$2

    players=("$arg1" "$arg2")

    if [ "$game_begin" = false ]; then
        random_player=($(shuf -e "${players[@]}"))
        selected_player="${random_player[0]}"
        game_begin=true
    else
        if [ "$selected_player" = "$arg1" ]; then
            selected_player="$arg2"
        else
            selected_player="$arg1"
        fi
    fi
}

check_winner(){
    player1=$1
    player2=$2
    player1_choice=$3
    player2_choice=$4

    case "$player1_choice$player2_choice" in
        "11" | "22" | "33" | "44")
            echo "Égalité !"
            ;;
        "13" | "21" | "32")
            echo "Le joueur $player1 gagne !"
            score_player1=$((score_player1 + 1))
            ;;
        "12" | "23" | "31")
            echo "Le joueur $player2 gagne !"
            score_player2=$((score_player2 + 1))
            ;;
        "14" | "24" | "34")
            echo "Le joueur $player2 gagne !"
            score_player2=$((score_player2 + 1))
            ;;
        "43" | "41" | "42")
            echo "Le joueur $player1 gagne !"
            score_player1=$((score_player1 + 1))
            ;;
        *)
            echo "Choix non valide, rejouez ce tour."
            ;;
    esac
}

rps(){
    game_begin=false
    score_player1=0
    score_player2=0

    echo "Nom du joueur 1 : "
    read player1

    echo "Nom du joueur 2 : "
    read player2

    while [ "$score_player1" -lt 3 ] && [ "$score_player2" -lt 3 ]; do
        who_begin "$player1" "$player2"
        echo "$player1 : score $score_player1"
        echo "$player2 : score $score_player2"
        echo "Au tour de $selected_player"
        while true; do
            echo "Pierre (1) / Feuille (2) / Ciseaux (3) / Ultime (4)"
            read -s choix

            if [ "$choix" -ge 1 ] && [ "$choix" -le 4 ]; then
                break 
            else
                echo "Choix invalide. Veuillez entrer 1, 2 ou 3."
            fi
        done

        if [ "$selected_player" = "$player1" ]; then
            player1_choice="$choix"
        else
            player2_choice="$choix"
        fi

        if [ -n "$player1_choice" ] && [ -n "$player2_choice" ]; then
            check_winner "$player1" "$player2" "$player1_choice" "$player2_choice"
            player1_choice=""
            player2_choice=""
        fi
    done

    if [ "$score_player1" -eq 3 ]; then
        echo "Le joueur $player1 a gagné !"
    else
        echo "Le joueur $player2 a gagné !"
    fi
}