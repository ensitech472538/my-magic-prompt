
## My Magic Prompt

Bienvenue dans My Magic Prompt, un shell personnalisé avec des fonctionnalités spéciales ! Pour accéder au prompt, suivez les étapes suivantes :

1.  Assurez-vous d'avoir le login et le mot de passe spécifiques.
    
2.  Le prompt doit être situé dans le dossier `~/my-magic-prompt/main.sh`.
    

### Commandes disponibles

Voici la liste des commandes que vous pouvez utiliser dans My Magic Prompt :

-   `help` : Affiche la liste des commandes disponibles.
-   `ls` : Liste les fichiers et les dossiers, y compris les fichiers cachés en utilisant les options.
-   `rm` : Supprime un fichier.
-   `rmd` ou `rmdir` : Supprime un dossier.
-   `about` : Affiche une description du programme.
-   `version` ou `--v` ou `vers` : Affiche la version du prompt.
-   `age` : Vous demande votre âge et vous indique si vous êtes majeur ou mineur.
-   `quit` : Permet de quitter le prompt.
-   `profil` : Affiche toutes les informations vous concernant (Prénom, Nom, Âge, Email).
-   `passw` : Vous permet de changer votre mot de passe avec une demande de confirmation.
-   `cd` : Vous permet de naviguer dans les dossiers, soit en créant un nouveau dossier, soit en revenant au dossier précédent.
-   `pwd` : Indique le répertoire actuel.
-   `hour` : Affiche l'heure actuelle.
-   `*` : Indique une commande inconnue.
-   `httpget` : Permet de télécharger le code source HTML d'une page web et de l'enregistrer dans un fichier spécifique, avec une demande du nom de fichier.
-   `smtp` : Vous permet d'envoyer un e-mail en spécifiant une adresse, un sujet et le corps du message.
-   `open` : Ouvre un fichier dans l'éditeur VIM, même s'il n'existe pas encore.

N'hésitez pas à explorer ces commandes pour tirer le meilleur parti de My Magic Prompt !